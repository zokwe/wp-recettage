<?php get_header(); ?>

<?php get_sidebar(); ?>

	<main role="main" class="col-12 col-sm-9">
		<!-- section -->
		<section>

			<h1><?php echo get_the_archive_title(); ?></h1>

            <h2>Liste des suggestions d'<?php echo single_term_title(); ?></h2>

			<div class="bg-light shadow-lg p-3 mb-5">
                <p><?php echo the_archive_description(); ?></p>
			</div>


			<?php get_template_part('loop-suggestion'); ?>

			<?php wpbootstrapsass_pagination(); ?>

		</section>
		<!-- /section -->
	</main>

<?php// get_footer(); ?>