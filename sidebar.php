<!-- sidebar -->
<aside id="webapp-sidebar" class="sidebar" role="complementary">


	<?php
	wp_nav_menu([
		'menu'            => 'sidebar-menu',
		'theme_location'  => 'sidebar-menu',
		'container'       => 'div',
		'container_id'    => '',
		'container_class' => 'collapse navbar-collapse',
		'menu_id'         => 'bs4navbar',
		'menu_class'      => 'nav justify-content-center',
		'depth'           => 2,
		'fallback_cb'     => 'bs4navwalker::fallback',
		'walker'          => new bs4navwalker()
	]);
	?>


	<?php// wpbootstrapsass_nav(); ?>

    <div class="sidebar-widget">
		<?php// if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-1')) ?>
    </div>

    <div class="sidebar-widget">
		<?php// if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-2')) ?>
    </div>

	<?php get_footer(); ?>

</aside>
<!-- /sidebar -->