<?php
/**
 * Template Name: Création rectte
 **/
 if ( is_user_logged_in() || current_user_can('publish_posts') ) { // Execute code if user is logged in
    acf_form_head();
    wp_deregister_style( 'wp-admin' );
}
 
 
?>

<?php acf_form_head(); ?>
<?php get_header(); ?>

<?php get_sidebar(); ?>

    <main role="main" class="col-12 col-sm-9">
        <!-- section -->
        <section>
        <?php while ( have_posts() ) : the_post(); ?>

            <h1><?php the_title(); ?></h1>
            

			<?php include 'forms/test-form.php'; ?>
        <?php endwhile; ?>

        </section>
        

    </main>


<?php// get_footer(); ?>
