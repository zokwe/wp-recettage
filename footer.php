            <!-- footer -->
            <footer class="footer d-flex align-items-center justify-content-center flex-wrap w-100" role="contentinfo">

                <!-- copyright -->

                    Copyright	&copy; <?php echo date('Y'); ?><br><?php bloginfo('name'); ?>

                <!-- /copyright -->

            </footer>
            <!-- /footer -->
         </div>
        <!-- /wrapper -->
        <?php wp_footer(); ?>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->

        <!-- analytics -->
        <script>
            (function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
                (f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
                l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
            ga('send', 'pageview');
        </script>

    </body>
</html>
