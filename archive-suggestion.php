<?php get_header(); ?>

<?php get_sidebar(); ?>

    <main role="main" class="col-12 col-sm-9">
        <!-- section -->
        <section>

            <h2><?php echo get_the_archive_title(); ?></h2>

            <div class="bg-light shadow-lg p-3 mb-5">
                <p>Cette rubrique archive sous forme de liste, toutes les suggestions émanant des différentes recettes et jeux de tests, sans distinction de catégorie de test, ni du produit du test etc. Cette liste est classée dans un ordre décroissant (de la plus récente à plus ancienne) et permet de savoir sur quel produit porte la suggestion et de quel type de suggestion il s'agit.</p>
            </div>


			<?php get_template_part('loop-suggestion'); ?>

			<?php wpbootstrapsass_pagination(); ?>

        </section>
        <!-- /section -->
    </main>

<?php// get_footer(); ?>