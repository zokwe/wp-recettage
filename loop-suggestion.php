
<?php if (have_posts()): ?>

<table class="table table-hover shadow-lg">

    <thead class="bg-light">
    <tr>
        <th scope="col">#</th>
        <th scope="col">Référence</th>
        <th scope="col">Produit</th>
        <th scope="col">Entité</th>
        <th scope="col">Type de suggestion</th>
        <th scope="col">Titre/sujet</th>
        <th scope="col">Validation</th>
        <th scope="col">Intégration</th>
    </tr>
    </thead>
    <tbody>

    <?php  while (have_posts()) : the_post(); ?>
        <tr onclick="window.location='<?php the_permalink(); ?>';" title="Cliquer pour consulter" class="row-click">
            <?php  $numerber = 1; ?>
            <td> <?php echo $numerber++; ?></td>
            <td>SUGG-<?php the_ID(); ?>-<?php echo the_time('Ymd');?></td>
            <td><?php the_field( 'produit'); ?></td>
            <td>
                <?php the_field( 'entite'); ?></td>
            <td><?php $titre = the_taxonomies(); echo $titre; ?></td>
            <td>
            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <!-- post title -->
                <h2>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                </h2>
                <!-- /post title -->
                <?php edit_post_link(); ?>

            </article>
            <!-- /article -->
            </td>
            <td><?php the_field('validation'); ?></td>
            <td><?php the_field('integration'); ?></td>

        </tr>
    <?php endwhile; ?>

    </tbody>

</table>

    <?php else: ?>

        <!-- article -->
        <article>
            <h2><?php _e( 'Sorry, nothing to display.', 'wpbootstrapsass' ); ?></h2>
        </article>
        <!-- /article -->

    <?php endif; ?>




