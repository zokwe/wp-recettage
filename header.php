<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>" href="<?php bloginfo('rss2_url'); ?>" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

        <link href="https://fonts.googleapis.com/css?family=Dosis:300,400|Jura:300,400,700" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/dist/fontawesome/all.min.css" rel="stylesheet" type="text/css">
        
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

		<?php wp_head(); ?>

	</head>
    <body <?php body_class(); ?>>

        <!-- header -->
        <header class="header fixed-top clear">

            <div class="header-inner d-flex flex-wrap align-items-center justify-content-start">
                <button class="hamburger hamburger--collapse" aria-label="Menu" data-toggle="collapse" data-target="#webapp-sidebar" aria-controls="webapp-sidebar" aria-expanded="false" aria-label="Menu">
                              <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                              </span><label>Menu</label>
                </button>
                <div class="brand-block col-sm-4 d-flex align-items-center justify-content-start">
			        <?php if ( function_exists( 'the_custom_logo' ) ): ?>
				        <?php the_custom_logo() ; ?>
			        <?php endif; ?>
                    <a class="brand ml-3" href="<?php site_url(); ?>">
                        <h1><?php echo get_bloginfo('name', 'display'); ?></h1>
                        <span><?php echo get_bloginfo('description', 'display'); ?></span>
                    </a>
                </div>
                <div class="col-sm-4 d-flex flex-wrap align-items-center justify-content-center">
                    <a href="#" class="btn btn-primary mr-md-5">Créer une recette</a>
			        <?php get_template_part('searchform'); ?>
                </div>
                <div class="wp-welcome-message d-flex align-items-center justify-content-center">
			        <?php if ( is_user_logged_in() ) {
				        $current_user = wp_get_current_user();
				        echo 'Bonjour, '.$current_user->display_name.' !';
			        } else {
				        echo 'Welcome, visitor!';
			        }
			        ?>

                    <a href="<?php echo site_url(); ?>/wp-login.php?action=logout" class="ml-5 float-right">
                        Quitter                    </a>
                </div>

            </div>

        </header>
        <!-- /header -->

        <!-- wrapper -->
        <div class="wrapper">
