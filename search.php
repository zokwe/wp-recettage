<?php get_header(); ?>
<?php get_sidebar(); ?>
<main>

    <div class="col-md-8">
        <!-- section -->
        <section>

            <h1 class="page-header"><?php echo sprintf( __( '%s Search Results for ', 'wpbootstrapsass' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

        </section>
        <!-- /section -->
    </div><!-- /.col-md-8 -->

</main>
<?php get_footer(); ?>
