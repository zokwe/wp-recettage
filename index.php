<?php get_header(); ?>


<?php get_sidebar(); ?>

<main role="main" class="col-12 col-sm-9">
    <!-- section -->
    <section>

        <h1><?php _e( 'Latest Posts', 'wpbootstrapsass' ); ?></h1>

		<?php get_template_part('loop'); ?>

		<?php get_template_part('pagination'); ?>

    </section>
    <!-- /section -->
</main>


<?php// get_footer(); ?>
