<?php
/**
 * Template Name: Jeux de tests
 **/
?>


<?php get_header(); ?>

<?php get_sidebar(); ?>

    <main role="main" class="col-12 col-sm-9">
        <!-- section -->
        <section>

            <h1><?php the_title(); ?></h1>

			<?php include 'template-parts/content-jeux-de-tests.php'; ?>

        </section>
        <!-- /section -->

    </main>


<?php// get_footer(); ?>
