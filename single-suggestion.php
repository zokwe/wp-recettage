<?php get_header(); ?>


<?php get_sidebar(); ?>

    <main role="main" class="col-12 col-sm-9">
        <!-- section -->
        <section>

			<?php if (have_posts()): while (have_posts()) : the_post(); ?>

                <!-- article -->
                <div class="single-sugg d-flex align-items-start justify-content-start flex-wrap">
                    <div class="mr-3">Référence</div><div><p>SUGG-<?php the_ID(); ?>-<?php echo date('Ymd');?></p></div>
                    <div>
                        <h3><?php the_title(); ?></h3>
                        <!-- post thumbnail -->
						<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
							<a href="#exampleModalCenter-<?php echo get_post_thumbnail_id(); ?>" title="<?php the_title(); ?>" data-toggle="modal" data-target="#exampleModalCenter-<?php echo get_post_thumbnail_id(); ?>">
								<?php the_post_thumbnail('large', ['class' => 'img-fluid']); // Fullsize image for the single post ?>
							</a>
							<div class="modal fade" id="exampleModalCenter-<?php echo get_post_thumbnail_id(); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenter-<?php echo get_post_thumbnail_id(); ?>" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                                        </div>

                                        
                                    </div>
                                </div>
                                
                            </div>
							<hr>
						<?php endif; ?>
						<!-- /post thumbnail -->
                        <p><?php the_content(); ?></p>
                        <?php edit_post_link(); // Always handy to have Edit Post Links available ?>
                    </div>
                    <div>
                        <p><?php echo the_taxonomies(); ?>
                            <span class="date">
								Date du post : <?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?>
							</span>
                        </p>
                    </div>
                    <div class="col-12 border-top">
                        <?php comments_template(); ?>
                    </div>

                </div>
                <hr>
			<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>

			<?php else: ?>

                <!-- article -->
                <article>

                    <h1><?php _e( 'Sorry, nothing to display.', 'wpbootstrapsass' ); ?></h1>

                </article>
                <!-- /article -->

			<?php endif; ?>

        </section>
        <!-- /section -->
    </main>

<?php get_footer(); ?>
