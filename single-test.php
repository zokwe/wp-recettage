<?php get_header(); ?>


<?php get_sidebar(); ?>

    <main role="main" class="col-12 col-sm-9">
        <!-- section -->
        <section>
        <?php $terms = get_the_terms( $post->ID, 'type_test' ); ?>
        <?php if ( !empty( $terms ) ): $term = array_shift( $terms ); ?>
                <?php $type = $term->slug; ?>
                <?php if($type == "recette-fonctionnelle"): ?>

                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                        <!-- article -->
                        <?php include 'template-parts/content-single-test.php'; ?>

                    <?php endwhile; ?>

                    <?php else: ?>

                        <!-- article -->
                        <article>

                            <h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

                        </article>
                        <!-- /article -->

                    <?php endif; ?>
                <?php elseif($type == "recette-responsivite"): ?>
        
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                        <!-- article -->
                        <?php include 'template-parts/content-single-test-responsivite.php'; ?>

                    <?php endwhile; ?>

                    <?php else: ?>

                        <!-- article -->
                        <article>

                            <h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

                        </article>
                        <!-- /article -->

                    <?php endif; ?>
                <?php elseif($type == "recette-technique"): ?>
        
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                        <!-- article -->
                        <?php include 'template-parts/content-single-test-technique.php'; ?>

                    <?php endwhile; ?>

                    <?php else: ?>

                        <!-- article -->
                        <article>

                            <h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

                        </article>
                        <!-- /article -->

                    <?php endif; ?>
                <?php elseif($type == "recette-accessibilite"): ?>
        
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                        <!-- article -->
                        <?php include 'template-parts/content-single-test-accessibilite.php'; ?>

                    <?php endwhile; ?>

                    <?php else: ?>

                        <!-- article -->
                        <article>

                            <h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

                        </article>
                        <!-- /article -->

                    <?php endif; ?>
                <?php endif; ?>

        <?php endif; ?>

        </section>
        <!-- /section -->
    </main>

<?php// get_footer(); ?>
