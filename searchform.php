<!-- search -->
<form class="search form-inline align-item-center justify-content-start" method="get" action="<?php echo home_url(); ?>" role="search">
    <input class="search-input form-control mr-3" type="search" name="s" placeholder="<?php _e( 'To search, type and hit enter.', 'wpbootstrapsass' ); ?>">
    <button class="search-submit btn btn-primary d-flex align-items-center justify-content-center pt-0 pb-0" type="submit" role="button"><?php _e( 'Search', 'wpbootstrapsass' ); ?></button>
</form>
<!-- /search -->
