<?php get_header(); ?>


<?php get_sidebar(); ?>

    <main role="main" class="col-12 col-sm-9">
        <!-- section -->
        <section>

            <h1>Archives des recettes pour le <?php echo the_archive_title(); ?></h1>

			<?php get_template_part('loop-test'); ?>

        </section>
        <!-- /section -->
    </main>

<?php// get_footer(); ?>
