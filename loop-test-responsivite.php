<table class="table table-hover">
    <thead>
    <tr class="table-primary">
        <th scope="col">Référence</th>
        <th scope="col">Produit</th>
        <th scope="col">Entité</th>
        <th scope="col">Workflow</th>
        <th scope="col">Date de création</th>
        <th scope="col">Recetteur</th>
        <th scope="col">Statut</th>
        <th scope="col">Date clôture</th>
    </tr>
    </thead>
    <tbody>
    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <tr onclick="window.location='<?php the_permalink(); ?>';" title="Cliquer pour consulter" class="row-click">
                <th scope="row">
	                <?php
	                // vars
	                $ref_rect = get_field('reference_recette');
	                if( $ref_rect ): ?>
                        <?php echo $ref_rect['code_type_recette']; ?>-<?php echo the_field('produit_recette'); ?>-<?php echo $ref_rect['code_date_rea']; ?>-<?php echo the_ID(); ?>
                     <?php endif; ?>
                </th>
                <td><?php the_field('produit_recette'); ?></td>
                <td><?php the_field('entite'); ?></td>
                <td><?php the_title(); ?></td>
                <td><span class="date"><?php the_field( 'date_rea'); ?></td>
                <td><span class="author"><?php the_author(); ?></td>
                <td class="statut-recette"><?php the_field('status_recette'); ?></td>
                <td><?php the_field('date_cloture'); ?></td>
            </tr>
        </article>

    <?php endwhile; ?>

        <!-- reset the main query loop -->
	    <?php wp_reset_postdata(); ?>

    <?php else: ?>

        <!-- article -->
        <article>
            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
        </article>
        <!-- /article -->
    <?php endif; ?>

    </tbody>
</table>
