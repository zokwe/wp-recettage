<?php
/**
 * Template Name: Home Page
 **/
?>


<?php get_header(); ?>

<?php get_sidebar(); ?>

    <main role="main" class="col-12 col-sm-9">
        <!-- section -->
        <section>

            <h1><?php the_title(); ?></h1>

			<?php include 'template-parts/content-home-page.php'; ?>

        </section>
        <!-- /section -->
        <section>

            <h2><?php the_title(); ?></h2>

            <div class="totalhome">
				<?php
				// status completed
				/*$args = array(
					'post_type' => 'recette',
					'post_status' => 'publish', // or whatever
					// other WP_Query arguments as needed
					'fields' => 'ids', // return only post IDs, since we just need to get a count
					'meta_query' => array(
						array(
							'key' => 'status',
							'value' => 'complete'
						)
					)
				);
				$results = new WP_Query($args);
				$completed = count($results->posts);*/
				?>

                <p>Nombre de recettes réalisées à ce jour : <?php echo $completed; ?></p>
            </div>

        </section>
        <!-- /section -->

    </main>


<?php// get_footer(); ?>