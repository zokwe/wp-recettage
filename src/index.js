import $ from "jquery";
import "popper.js";
import "bootstrap";
// Can import modules as needed
// ex. import 'bootstrap/js/dist/dropdown';
import "./sass/style.scss";
import "animate.css/animate.min.css";

"use strict";

(function($) {
	$(function() {
		console.log("Document Ready from index.js in the src root directory");
	});
})(jQuery);


import "./js/custom.js";