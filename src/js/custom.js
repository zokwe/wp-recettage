(function($) {
    $(function() {
        console.log("Document Ready from custom.js in src/js directory");
        $(document).ready(function () {
            'use strict';
            //jQuery.noConflict();

            //alert("oui je sui là");

            // DOM ready, take it away


            //hamburger menu
            // Look for .hamburger
            var $hamburger = $(".hamburger");
            $hamburger.on("click", function(e) {
                $hamburger.toggleClass("is-active");
                // Do something else, like open/close menu
                $("#webapp-sidebar").toggleClass("expanded");
                $("main").toggleClass("pushedright");
            });

            var sidebar_on = $("#webapp-sidebar");
            sidebar_on.hover(function () {
            $("main").toggleClass("pushedright");
            });


            // accordion fireup
            $('.collapse').collapse();

            //couleurs td
            $(".indicator:contains('Bloquant')").each(
                function()
                { $(this).addClass("bg-danger"); }
            );

            $(".indicator:contains('Non bloquant')").each(
                function()
                { $(this).addClass("bg-warning"); }
            );

            $(".indicator:contains('Améliorer')").each(
                function()
                { $(this).addClass("bg-info"); }
            );

            $(".indicator:contains('Succès')").each(
                function()
                { $(this).addClass("bg-success"); }
            );

            $(".statut-recette:contains('Cloturée')").each(
                function()
                { $(this).addClass("bg-success"); }
            );

            $(".statut-recette:not(:contains('Cloturée'))").each(function () {
                $(this).addClass("bg-warning");
            });

        });


    });
})(jQuery);
