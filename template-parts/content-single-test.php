<article <?php post_class(); ?>

<header>
	<h2 class="entry-title"><?php echo the_title(); ?></h2>
</header>
<div class="entry-content">
	<?php include 'subparts/test-header.php'; ?>
	<?php include 'subparts/test-main-content.php'; ?>
	<?php include 'subparts/test-dependences.php'; ?>
	<?php include 'subparts/test-dependences-secondaires.php'; ?>
	<?php include 'subparts/test-traductions.php'; ?>
    <?php include 'subparts/test-galerie-images.php'; ?>
	<?php include 'subparts/test-suggestions.php'; ?>
</div>
<?php edit_post_link(); // Always handy to have Edit Post Links available ?>
</article>
