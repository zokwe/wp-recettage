<h3>Traductions</h3>
<table class="table table-bordered shadow mb-5">

    <tbody>

	<?php

	/*
	* Loop through a repeater for each role*/

	if(get_field('traductions')): ?>

	<?php while(have_rows('traductions')): the_row(); ?>

    <thead class="table bg-light"">
    <tr>
        <th scope="col" colspan="8" class="text-center bg-light border-top-0">

			<?php the_sub_field('roles'); ?>
        </th>

    </tr>
    <tr>
        <th scope="col">Sens de la traduction</th>
        <th scope="col">Traduction actuelle</th>
        <th scope="col">Nouvelle traduction</th>
        <th scope="col">Bug</th>
        <th scope="col">Ticket Bug</th>
        <th scope="col">Indicateur</th>
        <th scope="col">Statut du bug</th>
        <th scope="col">Date résolution</th>
    </tr>
    </thead>
    <tr >


		<?php if( have_rows('groupe_test') ):

		while( have_rows('groupe_test') ): the_row();

		// vars
		//$attendu = get_sub_field('resultat_attendu');
		//$constate = get_sub_field('resultat_constate');
		?>
    <tr>

        <td class="border-top-0">
			<?php the_sub_field('sens_de_la_traduction'); ?>
        </td>
        <td class="border-top-0">
			<?php the_sub_field('traduction_actuelle'); ?>
        </td>
        <td class="border-top-0">
			<?php the_sub_field('nouvelle_traduction_proposee'); ?>
        </td>
        <td>
			<?php the_sub_field('bug'); ?>
        </td>
        <td>
			<?php the_sub_field('ticket_bug'); ?>
        </td>
        <td class="indicator">
			<?php the_sub_field('indicateur'); ?>
        </td>
        <td>
			<?php the_sub_field('statut_bug'); ?>
        </td>
        <td>
			<?php the_sub_field('date_de_resolution'); ?>
        </td>
    </tr>

	<?php endwhile; ?>
	<?php endif; ?>

    </tr>


	<?php endwhile; ?>

	<?php endif; ?>

    </tbody>
</table>
