<div class="test-section-wrap d-flex align-items-center justify-content-start flex-wrap col-12 border pl-0 pr-0 mt-3 mb-3">
	<div class="section-title bg-light col-12 col-lg-3 d-flex align-items-center justify-content-center border">
		<p>Suggestions d'évelution</p>
	</div>

	<div class="col-12 col-lg-9">
		<div class="row header-row">
			<div class="section-title bg-light col-6 col-lg-9">
				<p>Remarques</p>
			</div>
			<div class="section-title bg-light col-6 col-lg-3">
				<p>Type</p>
			</div>
		</div>
		<div class="row">
			<?php if( have_rows('suggestions') ): ?>

            <?php  $numerber = 1; ?>

				<?php while( have_rows('suggestions') ): the_row(); ?>

					<?php $post_object = get_sub_field('liens_suggestions'); ?>
					<?php if( $post_object ): ?>
						<?php
						// override $post
						$post = $post_object;
						setup_postdata( $post );
						?>
                        <div class="col-2">
                            <p><?php echo $numerber++; ?></p>
                        </div>
                        <div class="section-title bg-light col-7">
                            <div class="accordion" id="accordionExample-">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne-<?php the_ID(); ?>" aria-expanded="false" aria-controls="collapseOne-<?php the_ID(); ?>">
                                                <h3><?php the_title(); ?></h3>
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="collapseOne-<?php the_ID(); ?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="suggref">SUGG-<?php the_ID(); ?>-<?php echo date('Ymd');?></div>
                                            <p><?php the_content(); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="section-title bg-light col-3">
                            <p><?php echo the_taxonomies(); ?></p>
                        </div>

						<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

					<?php endif; ?>
				<?php endwhile; ?>

			<?php endif; ?>



		</div>

	</div>

</div>