<h3>Test des dépendences directes de la  fonctionnalité principale</h3>
<table class="table table-bordered shadow mb-5">

    <tbody>

	<?php

	/*
	* Loop through a repeater for each role*/

	if(get_field('test_dependences_actions')): ?>

	<?php while(have_rows('test_dependences_actions')): the_row(); ?>
    <thead class="table bg-light"">
    <tr>
        <th scope="col" colspan="7" class="text-center bg-light border-top-0">

			<?php the_sub_field('roles'); ?>
        </th>

    </tr>
    <tr>
        <th scope="col">Résultat attendu</th>
        <th scope="col">Constat</th>
        <th scope="col">Bug</th>
        <th scope="col">Ticket Bug</th>
        <th scope="col">Indicateur</th>
        <th scope="col">Statut du bug</th>
        <th scope="col">Date résolution</th>
    </tr>
    </thead>


    <tr >


		<?php if( have_rows('groupe_test') ):

		while( have_rows('groupe_test') ): the_row();

		// vars
		//$attendu = get_sub_field('resultat_attendu');
		//$constate = get_sub_field('resultat_constate');
		?>
    <tr>

        <td class="border-top-0">
			<?php the_sub_field('resultat_principal_attendu'); ?>
        </td>
        <td class="border-top-0">
			<?php the_sub_field('resultat_principal_constate'); ?>
        </td>
        <td>
			<?php the_sub_field('bug'); ?>
        </td>
        <td>
			<?php the_sub_field('ticket_bug'); ?>
        </td>
        <td class="indicator">
			<?php the_sub_field('indicateur'); ?>
        </td>
        <td>
			<?php the_sub_field('statut_bug'); ?>
        </td>
        <td>
			<?php the_sub_field('date_de_resolution'); ?>
        </td>
    </tr>

	<?php endwhile; ?>
	<?php endif; ?>

    </tr>


	<?php endwhile; ?>

	<?php endif; ?>

    </tbody>

</table>
