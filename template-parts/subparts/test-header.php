<div class="recette_header d-flex flex-wrap mb-5">
    <div class="col-md-6">
        <table class="table table-bordered bg-light shadow-lg">
            <tr class="pl-3">
				<?php

				// vars
				$ref_rect = get_field('reference_recette');

				if( $ref_rect ): ?>
                    <th>Référence de la recette</th>
                    <td><?php echo $ref_rect['code_type_recette']; ?>-<?php echo the_field('produit_recette'); ?>-<?php echo $ref_rect['code_date_rea']; ?>-<?php echo the_ID(); ?>
                    </td>
				<?php endif; ?>
            </tr>
            <tr class="pl-3">
                <th>Produit</th>
                <td><?php echo the_field('produit_recette'); ?></td>
            </tr>
            <tr class="pl-3">
                <th>Entité ciblée</th>
                <td><?php the_field('entite'); ?></td>
            </tr>
            <tr class="pl-3">
                <th>Workflow</th>
                <td><?php the_field('workflow'); ?></td>
            </tr>
            <tr class="pl-3">
                <th>Date de réalisation</th>
                <td><?php the_field('date_rea'); ?></td>
            </tr>
            <tr class="pl-3">
                <th>Statut</th>
                <td class="statut-recette"><?php the_field('status_recette'); ?></td>
            </tr>
            <tr class="pl-3">
                <th>Recetteur</th>
                <td><?php the_author(); ?><span class="badge badge-primary badge-pill ml-3"><?php the_author_posts(); ?></span></td>
            </tr>
        </table>

    </div>
    <div class="col-md-6">
        <div class="d-flex flex-wrap mb-3">
            <div class="col-2 bg-danger"></div>
            <div class="col-10">Dysfonctionnement critique ou bloquant</div>
        </div>
        <div class="d-flex flex-wrap mb-3">
            <div class="col-2 bg-warning"></div>
            <div class="col-10">Dysfonctionnement non bloquant</div>
        </div>
        <div class="d-flex flex-wrap mb-3">
            <div class="col-2 bg-info"></div>
            <div class="col-10">A améliorer : répond partiellement aux attendus</div>
        </div>
        <div class="d-flex flex-wrap mb-3">
            <div class="col-2 bg-success"></div>
            <div class="col-10">Satisfaisant : répond parfaitement aux attendus</div>
        </div>

    </div>


</div>
