<h3>Galerie Images bug</h3>

<?php

$images = get_field('galerie_image');

if( $images ): ?>
	<div class="galerie d-flex align-items-start justify-content-start flex-wrap col-12">
		<?php foreach( $images as $image ): ?>
			<div class="col-12 col-md-6 col-lg-3" data-toggle="modal" data-target="#exampleModalCenter-<?php echo $image['ID']; ?>">
				<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
				<!-- Modal -->
				<div class="modal fade" id="exampleModalCenter-<?php echo $image['ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-<?php echo $image['ID']; ?>" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<button type="button" class="close" data-dismiss="modal" aria-label="Fermer">
								<span aria-hidden="true">X Fermer</span>
							</button>

                            <div class="modal-body">
                                <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                                <?php the_post_thumbnail(); echo get_post(get_post_thumbnail_id())->post_content; ?>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
