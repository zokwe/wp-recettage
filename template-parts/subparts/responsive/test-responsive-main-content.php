<div class="d-flex align-items-start justify-content-start d-flex p-3 bg-light shadow-lg mb-4">
    <div class="">
        <h3>Test responsive : Scénario testé</h3>
        <p><?php the_field('scenario'); ?></p>
    </div>
</div>
<h3>Test de fonctionnalité principale</h3>
<h4>Workflow testé : <?php the_field('workflow'); ?></h4>
<table class="table table-bordered shadow mb-5">

    <thead class="bg-light">
    <tr>
        <th scope="col">Référence</th>
        <th scope="col">Produit</th>
        <th scope="col">Entité</th>
        <th scope="col">Action</th>
        <th scope="col">Profils impliqués</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
			<?php
			// vars
			$ref_rect = get_field('reference_recette');
			if( $ref_rect ): ?>
				<?php echo $ref_rect['code_type_recette']; ?>-<?php echo the_field('produit_recette'); ?>-<?php echo $ref_rect['code_date_rea']; ?>-<?php echo the_ID(); ?>
			<?php endif; ?>
        </td>
        <td>
			<?php the_field( 'produit_recette'); ?>
        </td>
        <td>
			<?php the_field( 'entite'); ?>
        </td>
        <td>
			<?php the_field( 'description_action'); ?>
        </td>
        <td class="pl-4">
            <ul>
				<?php

				/*
				* Loop through a repeater field
				*/
				

				if(get_field('roles_impliques')): ?>

					<?php while(the_repeater_field('roles_impliques')): ?>

                        <li><?php the_sub_field('roles'); ?></li>

					<?php endwhile; ?>

				<?php endif; ?>
            </ul>

        </td>
    </tr>

    </tbody>
</table>

<table class="table table-bordered shadow mb-5">

    <tbody>

	<?php

	/*
	* Loop through a repeater for each role*/

	if(get_field('test_responsive_principal')): ?>

	<?php while(have_rows('test_responsive_principal')): the_row(); ?>

    <thead class="table bg-light"">
    <tr>
        <th scope="col" colspan="9" class="text-center bg-light border-top-0">

			<?php the_sub_field('roles'); ?>

        </th>
    </tr>
    <tr>
        <th scope="col">Eléments</th>
        <th scope="col">Viewport</th>
        <th scope="col">Résultat attendu</th>
        <th scope="col">Constat</th>
        <th scope="col">Bug</th>
        <th scope="col">Ticket Bug</th>
        <th scope="col">Indicateur</th>
        <th scope="col">Statut du bug</th>
        <th scope="col">Date résolution</th>
    </tr>
    </thead>


    <tr >


		<?php if( have_rows('groupe_test') ):

		while( have_rows('groupe_test') ): the_row();
		// vars
		//$attendu = get_sub_field('resultat_attendu');
		//$constate = get_sub_field('resultat_constate');
		?>
    <tr>

        <td class="border-top-0">
			<?php the_sub_field('elements_visuels'); ?>
        </td>
        <td class="border-top-0">
			<?php the_sub_field('viewport'); ?>
        </td>
        <td class="border-top-0">
			<?php the_sub_field('resultat_principal_attendu'); ?>
        </td>
        <td class="border-top-0">
			<?php the_sub_field('resultat_principal_constate'); ?>
        </td>
        <td>
			<?php the_sub_field('bug'); ?>
        </td>
        <td>
			<?php the_sub_field('ticket_bug'); ?>
        </td>
        <td class="indicator text-center">
			<?php the_sub_field('indicateur'); ?>
        </td>
        <td>
			<?php the_sub_field('statut_bug'); ?>
        </td>
        <td>
			<?php the_sub_field('date_de_resolution'); ?>
        </td>
    </tr>

	<?php endwhile; ?>
	<?php endif; ?>

    </tr>


	<?php endwhile; ?>

	<?php endif; ?>

    </tbody>

</table>

