<?php
$test_suggestions = get_field_object( 'suggestions'); ?>

<h3><?php echo $test_suggestions['label']; ?></h3>



<?php if( have_rows('suggestions') ): ?>

    <div class="test-sugg-wrap d-flex flex-wrap">

		<?php while( have_rows('suggestions') ): the_row(); ?>

			<?php

			$post_object = get_sub_field('liens_suggestions');

			if( $post_object ):

				// override $post
				$post = $post_object;
				setup_postdata( $post );

				?>

                <div class="card col-12 col-md-6">

                    <div class="card-body">
                        <div class="card-header">
                            <h4><?php the_title(); ?></h4>
                        </div>
                        <p><?php wpbootstrapsass_excerpt('wpbootstrapsass_custom_post_20'); ?></p>

                        <div class="card-footer d-flex align-items-center justify-content-center flex-wrap">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter-<?php echo the_ID(); ?>">
                                Apperçu
                            </button>
                        </div> <!-- Button trigger modal -->

                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalCenter-<?php echo the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle-<?php echo the_ID(); ?>" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle"><?php the_title(); ?></h5>
                                    
                                </div>
                                <div class="modal-body">
                                    <p><?php the_content(); ?></p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                    <a href="<?php the_permalink(); ?>" class="btn btn-primary">Lire en détail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- si besoin on peut obtenir et afficher un champ acf du post appelé
                        <span>Post Object Custom Field: <?php// the_field('field_name'); ?></span>
                        -->

				<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>


		<?php endwhile; ?>
    </div>

<?php else: ?>
    <article>
        <p>Aucun contenu trouvé.</p>
    </article>
<?php endif; ?>
