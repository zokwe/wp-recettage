<section>
	<h3>Statistiques des recettes fonctionnelles par type de recette</h3>
	<div class="wrap d-flex align-items-center justify-content-center flex-wrap b-3 mt-3">

		<?php
		$terms = get_terms( array(
			'post_type' => 'test',
			'taxonomy' => 'type_test',
			'hide_empty' => false,
		) );
		?>
		<?php foreach($terms as $term): ?>

			<div class="card col-12 col-md-6 mt-3 mb-3">

				<div class="card-body shadow-lg">
					<img src="/wp-content/uploads/kisspng-speedometers-computer-icons.png" class="card-img-top" alt="...">
					<h5 class="card-title"><?php echo $term->name; ?></h5>
					<p class="card-text">Description courte de "<?php echo $term->name; ?>".</p>

					<h6>Avancement</h6>
					<?php
					$args = array(
						'post_type' => 'test',
						'posts_per_page' => -1,  //show all posts
						'tax_query' => array(
							array(
								'taxonomy' => 'type_test',
								'field' => 'slug',
								'terms' => $term->slug,
							)
						)
					);
					$loop = new WP_Query($args);
					?>

					<?php
					// my loop
					if ( $loop->have_posts() ) : ?>

						<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<?php// the_title(); //afficher les titres des posts correspondant pour test seulement ?>
							<?php
							//status completed or different from A faire
							$args = array(
								'post_type' => 'test',
								'post_status' => 'publish', // or whatever
								// other WP_Query arguments as needed
								'fields' => 'ids', // return only post IDs, since we just need to get a count
								'relation' => 'AND', // both of below conditions must match
								'tax_query' => array(
									array(
										'taxonomy' => 'type_test',
										'field' => 'slug',
										'terms' => $term->slug,
									)
								),
								'meta_query' => array(
									array(
										'key' => 'status_recette',
										'value' => 'A faire',
										'compare'=> '!='
									)
								)
							);
							$results = new WP_Query($args);
							$completed = count($results->posts);

							?>
							<?php
							// nombre d'entités
							$field_key = "field_5c3b470772eb4";
							$field = get_field_object($field_key);
							$choix = $field['choices'];
							$nombre_choix = count($choix);
							//if( $field )
							//echo $nombre_choix;
							?>

							<?php
							// nombre cycle_de_vie
							$field_key = "field_5c3b492cbf392";
							$field = get_field_object($field_key);
							$choix = $field['choices'];
							$nombre_cycledevie = count($choix);
							//if( $field )
							//echo $nombre_choix;
							?>

							<?php
							// nombre type d'actions possibles
							$field_key = "field_5c4849b930b45";
							$field = get_field_object($field_key);
							$choix = $field['choices'];
							$nombre_typeactions = count($choix);
							//if( $field )
							//echo $nombre_choix;
							?>
						<?php endwhile; ?>
						<?php wp_reset_query(); ?>

					<?php endif; ?>


					<?php/*
					// nombre d'entités
					$field_key = "field_5c3b470772eb4";
					$field = get_field_object($field_key);
					$choix = $field['choices'];
					$nombre_choix = count($choix);
					//if( $field )
					//echo $nombre_choix;
					?>

					<?php
					// nombre cycle_de_vie
					$field_key = "field_5c3b492cbf392";
					$field = get_field_object($field_key);
					$choix = $field['choices'];
					$nombre_cycledevie = count($choix);
					//if( $field )
					//echo $nombre_choix;
					?>

					<?php
					// nombre type d'actions possibles
					$field_key = "field_5c4849b930b45";
					$field = get_field_object($field_key);
					$choix = $field['choices'];
					$nombre_typeactions = count($choix);
					//if( $field )
					//echo $nombre_choix;*/
					?>

					<?php $pourcentage_avancement = ($completed / ($nombre_choix*$nombre_cycledevie*$nombre_typeactions))*100; ?>
					<?php $pourct_snsvirgule = number_format_i18n( $pourcentage_avancement); ?>


					<p>Nombre total d'entités : <?php echo $nombre_choix; ?></p>
					<p>Nombre de recettes réalisées à ce jour : <?php echo $completed; ?></p>
					<div class="progress mb-5" style="height: 20px;">
						<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: <?php echo $pourcentage_avancement; ?>%" aria-valuenow="<?php echo $pourct_snsvirgule; ?>%" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
					<div class="card-footer d-flex align-items-center justify-content-center d-print-flex">
						<a href="<?php echo site_url().'/'.'type_test'.'/'.$term->slug; ?>" class="btn btn-primary">Consulter</a>
					</div>

				</div><!--end card- body-->
			</div><!--end card -->
		<?php endforeach; ?>

	</div>

</section>