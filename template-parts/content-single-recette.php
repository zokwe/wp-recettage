<article <?php post_class(); ?>
	<header>
		<h1 class="entry-title"><?php echo the_title(); ?></h1>
        <h2>Hello</h2>
	</header>
	<div class="entry-content">
		<div class="recette_header">
			<div class="col-12 col-md-6 d-flex flex-wrap border">

				<div class="d-flex flex-wrap col-12">
					<?php $field = get_field_object('reference_recette') ; ?>
					<?php if(get_field('reference_recette')): ?>
					<div class="col">
						<?php echo($field['label']) ; ?> :
					</div>
					<div class="col">
						<?php $champ = get_field('reference_recette') ; ?>
						<?php echo($champ['code_recette']) ; ?> -
					</div>
					<?php endif; ?>
				</div>

				<div class="d-flex flex-wrap col-12">
					<?php if(get_field('produit_recette')): ?>
					<?php $field = get_field_object('produit_recette') ; ?>
					<div class="col">
						<?php echo($field['label']) ; ?> :
					</div>
					<div class="col">
						<?php the_field('produit_recette') ; ?>
					</div>
					<?php endif; ?>
				</div>

				<div class="d-flex flex-wrap col-12">
					<?php if(get_field('entite')): ?>
					<?php $field = get_field_object('entite') ; ?>
					<div class="col">
						<?php echo($field['label']) ; ?> :
					</div>
					<div class="col">
						<?php the_field('entite') ; ?>
					</div>
					<?php endif; ?>
				</div>

				<div class="d-flex flex-wrap col-12">
					<?php if(get_field('cycle_de_vie')): ?>
					<?php $field = get_field_object('cycle_de_vie') ; ?>
					<div class="col">
						<?php echo($field['label']) ; ?> :
					</div>
					<div class="col">
						<?php the_field('cycle_de_vie') ; ?>
					</div>
					<?php endif; ?>
				</div>

				<div class="d-flex flex-wrap col-12">
					<?php if(get_field('workflow')): ?>
					<?php $field = get_field_object('workflow') ; ?>
					<div class="col">
						<?php echo($field['label']) ; ?> :
					</div>
					<div class="col">
						<?php the_field('workflow') ; ?>
					</div>
					<?php endif; ?>
				</div>

				<div class="d-flex flex-wrap col-12">
					<?php if(get_field('date_rea')): ?>
					<?php $field = get_field_object('date_rea') ; ?>
					<div class="col">
						<?php echo($field['label']) ; ?> :
					</div>
					<div class="col">
						<?php the_field('date_rea') ; ?>
					</div>
					<?php endif; ?>
				</div>


				<div class="d-flex flex-wrap col-12">
					<?php if(get_field('status_recette')): ?>
                        <?php $field = get_field_object('status_recette') ; ?>
                        <div class="col">
                            <?php echo($field['label']) ; ?> :
                        </div>
                        <div class="col">
                            <?php the_field('status_recette') ; ?>
                        </div>
					<?php endif; ?>
				</div>

			</div>


		</div>
	</div>
</article>